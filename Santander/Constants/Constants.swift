//
//  Constants.swift
//  iOS-Test
//
//  Created by VP on 22/03/18.
//  Copyright © 2018 VP. All rights reserved.
//

import UIKit

enum NetworkingConstants {
    static let formCells = "https://floating-mountain-50292.herokuapp.com/cells.json"
    static let fund = "https://floating-mountain-50292.herokuapp.com/fund.json"
}

enum Color {
    //reds (primary app color)
    static let red = #colorLiteral(red: 0.8549019608, green: 0.003921568627, blue: 0.003921568627, alpha: 1)
    static let darkRed = #colorLiteral(red: 0.7843137255, green: 0.01568627451, blue: 0.01568627451, alpha: 1)
    static let inactiveRed = #colorLiteral(red: 0.9215686275, green: 0.462745098, blue: 0.462745098, alpha: 1)
    static let lineRed = #colorLiteral(red: 1, green: 0.1215686275, blue: 0.1215686275, alpha: 1)
    
    //gray scale
    static let white = #colorLiteral(red: 0.9999960065, green: 1, blue: 1, alpha: 1)
    static let lightGray = #colorLiteral(red: 0.937254902, green: 0.9333333333, blue: 0.9294117647, alpha: 1)
    static let gray = #colorLiteral(red: 0.6745098039, green: 0.6745098039, blue: 0.6745098039, alpha: 1)
    static let secondaryGray = #colorLiteral(red: 0.4941176471, green: 0.4941176471, blue: 0.4941176471, alpha: 1)
    static let black = #colorLiteral(red: 0.2, green: 0.2, blue: 0.2, alpha: 1)
    static let borderGray = #colorLiteral(red: 0.5921568627, green: 0.5921568627, blue: 0.5921568627, alpha: 1)
    
    //investment colors
    static let riskLightGreen = #colorLiteral(red: 0.4549019608, green: 0.8549019608, blue: 0.3803921569, alpha: 1)
    static let riskGreen = #colorLiteral(red: 0.2901960784, green: 0.7568627451, blue: 0.4235294118, alpha: 1)
    static let riskYellow = #colorLiteral(red: 1, green: 0.7529411765, blue: 0.06666666667, alpha: 1)
    static let riskOrange = #colorLiteral(red: 1, green: 0.4549019608, blue: 0.1725490196, alpha: 1)
    static let riskRed = #colorLiteral(red: 1, green: 0.2117647059, blue: 0.2039215686, alpha: 1)
}
