//
//  BaseViewController.swift
//  Santander
//
//  Created by VP on 4/21/18.
//  Copyright © 2018 VP. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {

    
    var rootController: CustomTabBarController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    func showLoader() {
        self.rootController?.showLoader()
    }
    
    func hideLoader() {
        self.rootController?.hideLoader()
    }
}
